import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/js/all.js';
import 'video.js/dist/video-js.css';
import { setupInterceptors } from './utils/setupInterceptors';

router.beforeEach((to, from, next) => {
  const authToken = localStorage.getItem('authtoken');
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!authToken) {
      next({
        params: { nextUrl: to.fullPath },
        name: 'Microsite',
      });
    } else {
      next();
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (to.query.city_code) {
      return next();
    }

    next({
      name: 'Login',
    });
  } else {
    next();
  }
});

setupInterceptors(store);

createApp(App).use(router).use(store).mount('#app');
