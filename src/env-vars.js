export const getLocationUrl = process.env.VUE_APP_API_URL + '/location';
export const getUserUrl = process.env.VUE_APP_API_URL + '/user';

const API_VERSION = 'v1';

export const WINSTON = {
  baseUrl: process.env.VUE_APP_API_URL + '/' + API_VERSION,
  API_X_TOKEN: process.env.VUE_APP_API_X_TOKEN,
  API_X_KEY_TOKEN: process.env.VUE_APP_API_X_KEY_TOKEN,
  WINSTON_URL: process.env.VUE_APP_WINSTON_URL,
  WINSTON_SITE_ID: process.env.VUE_APP_WINSTON_SITE_ID,
  WINSTON_REDIRECT_URL: process.env.VUE_APP_WINSTON_REDIRECT_URL,
};
