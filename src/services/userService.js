import { axios } from '../utils/axios';
export const userService = {
  async getUser() {
    try {
      const response = await axios.get(`/user/`);
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },

  async userSaveLocation(payload) {
    try {
      const response = await axios.post(`/user_saved_location/`, payload);
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },

  async deleteUserSaveLocation(payload) {
    try {
      const response = await axios.delete(`/user_saved_location/`, {
        data: { ...payload },
      });
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },

  async changeUserSavedLocation(payload) {
    try {
      const response = await axios.put(`/user_saved_location/change_save_location/`, payload);
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },

  async createUserEntry(payload) {
    try {
      const response = await axios.post(`/user/add_entry/`, payload);
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },

  async createUserLocationVisited(payload) {
    try {
      const response = await axios.post(`/user_location_visited/`, payload);
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },

  async createUserActivity(payload) {
    try {
      const response = await axios.post(`/user_activity`, payload);
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },

  async userUnlockedCity(payload) {
    try {
      const response = await axios.post('/user_unlocked_city/', payload);
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },
};
