import { axios } from '../utils/axios';
export const locationService = {
  async getLocations(payload) {
    try {
      const response = await axios.get(`/location`, {
        params: payload,
      });
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },
  async getLocationsByTime(payload) {
    try {
      const response = await axios.get(`/location/locations_by_time`, {
        params: payload,
      });
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },
};
