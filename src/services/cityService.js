import { axios } from '../utils/axios';

export const cityService = {
  async getCities(payload) {
    try {
      const response = await axios.get(`/city`, {
        params: payload,
      });
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },
};
