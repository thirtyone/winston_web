import { axios } from './../utils/axios';

export const winnerAnnouncementService = {
  async getWinnerAnnouncements(payload) {
    try {
      const response = await axios.get('/winner_announcement', {
        params: payload,
      });
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },
};
