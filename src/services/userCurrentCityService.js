import { axios } from '../utils/axios';
export const userCurrentCityService = {
  async changeUserCurrentCity(payload) {
    try {
      const response = await axios.put(`/user_current_city/change_current_city`, payload);
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },
};
