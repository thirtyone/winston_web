import { axios } from '../utils/axios';
export const userEntryService = {
  async getUserEntry(payload) {
    try {
      const results = await axios.get(`/user/user_entry`, {
        params: payload,
      });
      return results.data;
    } catch (err) {
      throw new Error(err);
    }
  },
};
