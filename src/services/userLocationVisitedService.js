import { axios } from '../utils/axios';
export const userLocationVisitedService = {
  async getUserLocationVisited(payload) {
    try {
      const response = await axios.get(`/user_location_visited`, {
        params: payload,
      });
      return response.data;
    } catch (err) {
      throw new Error(err);
    }
  },
};
