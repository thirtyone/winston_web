import { axios } from '../utils/axios';

export default {
  login(data) {
    return axios.post(`/auth/signin`, data);
  },
  async authenticate(payload) {
    const response = await axios.post(`/auth/authenticate`, payload);
    return response.data;
  },
  async refreshToken(payload) {
    const response = await axios.put('/auth/refresh_token', payload);
    return response.data;
  },
};
