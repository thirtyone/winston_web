import { useLocalStorage } from '../utils/localStorage';
import jwt_decode from 'jwt-decode';
const { getLocalStorage } = useLocalStorage();
export const tokenService = {
  getLocalAccessToken() {
    const accessToken = getLocalStorage('authtoken');
    return accessToken;
  },
  getLocalRefreshToken() {
    const refreshToken = getLocalStorage('refreshtoken');
    return refreshToken;
  },
  decodedRefreshToken() {
    const decodedToken = jwt_decode(this.getLocalRefreshToken());
    return decodedToken;
  },
};
