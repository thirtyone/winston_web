import { createRouter, createWebHistory } from 'vue-router';
import Login from '../views/Login.vue';
import Microsite from '../views/Microsite.vue';
import Home from '../views/Home.vue';
import Profile from '../views/Profile.vue';
import Faq from '../views/Faq.vue';
import Winners from '../views/Winners.vue';
import Location from '../views/Location.vue';
import Nav from '../components/Nav.vue';
import Starter from '../components/Starter.vue';
import Activities from '../components/Activities.vue';
import LocationVideo from '../views/LocationVideo.vue';
import LandingPage from '../views/LandingPage.vue';

const routes = [
  {
    path: '/',
    component: LandingPage,
    meta: {
      guest: true,
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/faq',
    name: 'Faq',
    component: Faq,
  },
  {
    path: '/winners',
    name: 'Winners',
    component: Winners,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/location',
    name: 'Location',
    component: Location,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/location-video',
    name: 'LocationVideo',
    component: LocationVideo,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/nav',
    name: 'Nav',
    component: Nav,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/activities',
    name: 'Activities',
    component: Activities,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/microsite',
    name: 'Microsite',
    component: Microsite,
  },
  {
    path: '/starter',
    component: Starter,
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
