import { useLocalStorage } from '../utils/localStorage';
import { useStore } from 'vuex';
import { computed } from 'vue';
const { getLocalStorage } = useLocalStorage();
export const useAuth = () => {
  const store = useStore();
  const isAuthenticated = !!getLocalStorage('authtoken');
  const isLoading = computed(() => store.state['auth'].isAuthApiLoading);
  const setAuthenticate = async ({ userid, token, success, fail }) => {
    await store.dispatch('auth/authenticate', {
      userid,
      token,
      success,
      fail,
    });
  };

  return {
    isAuthenticated,
    setAuthenticate,
    isLoading,
  };
};
