import { ref } from 'vue';
export const useActivity = () => {
  const activity = ref(null);
  const setSelectedActivity = (act) => {
    activity.value = act;
  };
  return {
    activity,
    setSelectedActivity,
  };
};
