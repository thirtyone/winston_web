import { userEntryService } from '../../services/userEntryService';

export default {
  state: {
    userEntries: [],
    isUserEntryLoading: false,
    isUserEntryError: false,
  },
  mutations: {
    setUserEntries(state, payload) {
      state.userEntries = payload;
    },
    setUserEntriesLoading(state, payload) {
      state.isUserEntryLoading = payload;
    },
    setUserEntriesError(state, payload) {
      state.isUserEntryError = payload;
    },
  },
  actions: {
    async getUserEntries({ commit }, payload) {
      try {
        commit('setUserEntriesLoading', true);
        const results = await userEntryService.getUserEntry(payload);
        const data = results?.data;
        commit('setUserEntries', data);
      } catch (err) {
        commit('setUserEntriesError', true);
      } finally {
        commit('setUserEntriesLoading', false);
      }
    },
  },
};
