import { cityService } from '@/services/cityService';
export default {
  state: {
    cities: [],
    isCityLoading: false,
    isCityError: false,
  },
  mutations: {
    setCityLoading(state, payload) {
      state.isCityLoading = payload;
    },
    setCityError(state, payload) {
      state.isCityError = payload;
    },
    getCities(state, payload) {
      state.cities = payload;
    },
  },
  actions: {
    async getCities({ commit }, payload) {
      try {
        commit('setCityLoading', true);
        const response = await cityService.getCities(payload);
        const { data } = response;

        commit('getCities', data);
      } catch (err) {
        commit('setCityError', true);
      } finally {
        commit('setCityLoading', false);
      }
    },
  },
};
