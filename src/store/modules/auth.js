import auth from '@/services/auth';
import { useLocalStorage } from '../../utils/localStorage';
const { setLocalStorage, removeLocalStorage } = useLocalStorage();
export default {
  namespaced: true,
  state: {
    auth: {},
    isAuthApiLoading: false,
    isAuthenticated: false,
  },
  mutations: {
    setAuth(state, data) {
      state.auth = data;
    },
    setIsAuthenticated(state, payload) {
      state.isAuthenticated = payload;
    },
    setAuthApiLoading(state, data) {
      state.isAuthApiLoading = data;
    },
    setCurrentAuth(state, data) {
      state.currentAuth = data;
    },
  },
  actions: {
    async login(context, { username, password, success, fail }) {
      context.commit('setAuthApiLoading', true);
      let resp;
      try {
        resp = await auth.login({ username, password });

        localStorage.setItem('authtoken', resp.data.token);
        setLocalStorage('refreshtoken', resp.data.refresh_token);
        if (success instanceof Function) {
          success();
        }
      } catch (e) {
        if (resp !== undefined && resp.status === 200) return false;
        if (fail instanceof Function) {
          fail();
        }
      } finally {
        context.commit('setAuthApiLoading', false);
      }
    },
    async authenticate({ commit }, { userid, token, success, fail }) {
      try {
        commit('setAuthApiLoading', true);
        const response = await auth.authenticate({ userid, token });
        const { token: jwtToken, refresh_token: refreshToken } = response;
        setLocalStorage('authtoken', jwtToken);
        setLocalStorage('refreshtoken', refreshToken);
        commit('setIsAuthenticated', true);
        success();
      } catch (err) {
        console.log('err', err);
        fail();
      } finally {
        commit('setAuthApiLoading', false);
      }
    },
    async refreshToken(_, payload) {
      try {
        const response = await auth.refreshToken(payload);
        const { token: jwtToken, refresh_token: refreshToken } = response;
        setLocalStorage('authtoken', jwtToken);
        setLocalStorage('refreshtoken', refreshToken);
      } catch (err) {
        console.log('refresh token error');
        removeLocalStorage('authtoken');
        removeLocalStorage('refreshtoken');
        window.location.href = '/login';
      }
    },
  },
};
