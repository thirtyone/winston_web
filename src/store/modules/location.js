import { locationService } from '@/services/locationService';

export default {
  state: {
    locations: [],
    locationOptions: [],
    isLocationLoading: false,
    isLocationError: false,
  },
  mutations: {
    setLocationLoading(state, data) {
      state.isLocationLoading = data;
    },
    setLocationError(state, data) {
      state.isLocationError = data;
    },
    getLocations(state, data) {
      state.locations = data;
    },
    getLocationOptions(state, data) {
      state.locationOptions = data;
    },

    setCurrentLocation(state, data) {
      state.currentLocation = data;
    },
    setCities(state, data) {
      state.cities = data;
    },
    setCurrentCity(state, data) {
      state.currentCity = data;
    },
  },
  actions: {
    async getLocations({ commit }, payload) {
      try {
        commit('setLocationLoading', true);
        const response = await locationService.getLocationsByTime(payload);
        const { data } = response;
        commit('getLocations', data);
      } catch (err) {
        commit('setLocationError', true);
      } finally {
        commit('setLocationLoading', false);
      }
    },
    async getLocationOptions({ commit }, payload) {
      try {
        commit('setLocationLoading', true);
        const response = await locationService.getLocationsByTime(payload);
        const { data } = response;
        commit('getLocationOptions', data);
      } catch (err) {
        commit('setLocationError', true);
      } finally {
        commit('setLocationLoading', false);
      }
    },
  },
};
