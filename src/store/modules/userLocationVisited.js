import { userLocationVisitedService } from '../../services/userLocationVisitedService';

export default {
  state: {
    userLocationVisited: [],
    isUserVisitedLocationLoading: false,
    isUserVisitedLocationError: false,
  },
  mutations: {
    setUserLocationVisited(state, payload) {
      state.userLocationVisited = payload;
    },
    setIsUserVisitedLocationLoading(state, payload) {
      state.isUserVisitedLocationLoading = payload;
    },
    setIsUserVisitedLocationError(state, payload) {
      state.isUserVisitedLocationError = payload;
    },
  },
  actions: {
    async getUserLocationVisited({ commit }, payload) {
      try {
        commit('setIsUserVisitedLocationLoading', true);
        const results = await userLocationVisitedService.getUserLocationVisited(payload);
        commit('setUserLocationVisited', results?.data);
      } catch (err) {
        commit('setIsUserVisitedLocationError', true);
      } finally {
        commit('setIsUserVisitedLocationLoading', false);
      }
    },
  },
};
