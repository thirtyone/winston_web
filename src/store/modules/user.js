import { userService } from '@/services/userService';

export default {
  state: {
    user: {},
    unlockedCities: [],
    userSavedLocations: [],
    userCurrentCity: {},
    userCurrentLocation: {},
    userActivities: [],
    isUserApiLoading: false,
    isUserError: false,
    isUnlockedCity: false,
  },
  mutations: {
    setUserApiLoading(state, data) {
      state.isUserApiLoading = data;
    },
    setUserError(state, data) {
      state.isUserError = data;
    },
    setUserDetails(state, data) {
      state.userDetails = data;
    },
    setUnlockedCities(state, data) {
      state.unlockedCities = data;
    },

    setSavedLocations(state, data) {
      state.userSavedLocations = data;
    },

    setIsUnlockedCity(state, payload) {
      state.isUnlockedCity = payload;
    },

    getUser(state, payload) {
      state.user = payload;
    },
    getUserCurrentCity(state, payload) {
      state.userCurrentCity = payload;
    },
    getUserCurrentLocation(state, payload) {
      state.userCurrentLocation = payload;
    },
    deleteUserSaveLocation(state) {
      state.userCurrentLocation = {};
    },
    getUserActivities(state) {
      state.userActivities = state;
    },
  },
  actions: {
    async getUser({ commit }) {
      try {
        commit('setUserApiLoading', true);
        const response = await userService.getUser();
        const { data } = response;
        const {
          unlocked_cities,
          user_entries,
          user_saved_locations,
          user_current_location,
          user_activities,
          user_current_city,
        } = data;
        commit('getUser', data);
        commit('setUnlockedCities', unlocked_cities);
        commit('setUserEntries', user_entries, { root: true });
        commit('setSavedLocations', user_saved_locations);
        commit('getUserActivities', user_activities);
        commit('getUserCurrentLocation', user_current_location);
        commit('getUserCurrentCity', user_current_city?.city);

        commit('setUserApiLoading', false);
      } catch (e) {
        console.log(e);
        commit('setUserApiLoading', false);
      }
    },

    async userSaveLocation({ commit }, payload) {
      try {
        commit('setUserApiLoading', true);
        const response = await userService.userSaveLocation(payload);
        commit('getUserCurrentLocation', response.data);
      } catch (err) {
        commit('setUserError', true);
      } finally {
        commit('setUserApiLoading', false);
      }
    },
    async deleteUserSaveLocation({ commit }, payload) {
      try {
        commit('setUserApiLoading', true);
        await userService.deleteUserSaveLocation(payload);
        commit('deleteUserSaveLocation');
      } catch (err) {
        commit('setUserError', true);
      } finally {
        commit('setUserApiLoading', false);
      }
    },
    async changeUserSavedLocation({ commit }, payload) {
      try {
        commit('setUserApiLoading', true);
        await userService.changeUserSavedLocation(payload);
      } catch (err) {
        commit('setUserError', true);
      } finally {
        commit('setUserApiLoading', false);
      }
    },
    async createUserEntry({ commit }, payload) {
      try {
        commit('setUserApiLoading', true);
        await userService.createUserEntry(payload);
      } catch (err) {
        commit('setUserError', true);
      } finally {
        commit('setUserApiLoading', false);
      }
    },
    async createUserLocationVisited({ commit }, payload) {
      try {
        commit('setUserApiLoading', true);
        await userService.createUserLocationVisited(payload);
      } catch (err) {
        commit('setUserError', true);
      } finally {
        commit('setUserApiLoading', false);
      }
    },
    async createUserActivity({ commit }, payload) {
      try {
        commit('setUserApiLoading', true);
        await userService.createUserActivity(payload);
      } catch (er) {
        commit('setUserError', true);
      } finally {
        commit('setUserApiLoading', false);
      }
    },
    async userUnlockedCity({ commit }, payload) {
      try {
        commit('setUserApiLoading', true);
        await userService.userUnlockedCity(payload);
        commit('setIsUnlockedCity', false);
      } catch (err) {
        commit('setUserError', true);
        commit('setIsUnlockedCity', true);
      } finally {
        commit('setUserApiLoading', false);
      }
    },
  },
};
