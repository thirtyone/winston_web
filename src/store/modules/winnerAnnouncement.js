import { winnerAnnouncementService } from '@/services/winnerAnnouncement';

export default {
  state: {
    winnerAnnouncements: [],
    isWinnerAnnouncementLoading: false,
    isWinnerAnnouncementError: false,
  },
  mutations: {
    setWinnerAnnouncementLoading(state, payload) {
      state.isWinnerAnnouncementLoading = payload;
    },
    setWinnerAnnouncementError(state, payload) {
      state.isWinnerAnnouncementError = payload;
    },
    getWinnerAnnouncements(state, payload) {
      state.winnerAnnouncements = payload;
    },
  },
  actions: {
    async getWinnerAnnouncements({ commit }, payload) {
      try {
        commit('setWinnerAnnouncementLoading', true);
        const response = await winnerAnnouncementService.getWinnerAnnouncements(payload);
        const { data } = response;
        commit('getWinnerAnnouncements', data);
      } catch (err) {
        commit('setWinnerAnnouncementError', true);
      } finally {
        commit('setWinnerAnnouncementLoading', false);
      }
    },
  },
};
