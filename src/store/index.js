import { createStore } from 'vuex';
import location from './modules/location';
import user from './modules/user';
import auth from './modules/auth';
import city from './modules/city';
import userEntry from './modules/userEntry';
import userLocationVisited from './modules/userLocationVisited';
import winnerAnnouncements from './modules/winnerAnnouncement';

const store = createStore({
  modules: {
    auth,
    user,
    userEntry,
    userLocationVisited,
    location,
    city,
    winnerAnnouncements,
  },
});

export default store;
