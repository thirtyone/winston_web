import { getHours } from 'date-fns';
export const getTime = () => {
  const currentTime = new Date();
  const getCurrentHour = getHours(currentTime);
  if (getCurrentHour >= 6 && getCurrentHour <= 17) {
    return 'DAY';
  }

  // SHOULD BE NIGHT
  return 'NIGHT';
};
