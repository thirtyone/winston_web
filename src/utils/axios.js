import Axios from 'axios';
import { WINSTON } from '../env-vars';

export const axios = Axios.create({
  baseURL: WINSTON.baseUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: '*/*',
  },
});
