import { WINSTON } from '../env-vars';
import { tokenService } from '../services/tokenService';
import { axios } from './axios';
export const setupInterceptors = (store) => {
  axios.interceptors.request.use(
    (config) => {
      const accessToken = tokenService.getLocalAccessToken();
      config.headers['API_SECRET_X_TOKEN'] = WINSTON.API_X_TOKEN;
      config.headers['API_SECRET_X_KEY_TOKEN'] = WINSTON.API_X_KEY_TOKEN;
      if (accessToken) {
        config.headers['Authorization'] = `Bearer ${accessToken}`;
      }
      return config;
    },
    (err) => {
      return Promise.reject(err);
    },
  );

  axios.interceptors.response.use(
    (response) => {
      if (response.status === 401) {
        //add your code
        alert('You are not authorized');
      }
      return response;
    },
    async (error) => {
      const originalConfig = error.config;

      if ((originalConfig.url !== '/auth/signin' || originalConfig.url !== '/auth/authenticate') && error.response) {
        if (error.response.status === 401 && !originalConfig._retry) {
          originalConfig._retry = true;
          try {
            const refreshToken = tokenService.getLocalRefreshToken();

            await store.dispatch('auth/refreshToken', {
              refresh_token: refreshToken,
            });
            return axios(originalConfig);
          } catch (err) {
            return Promise.reject(err);
          }
        }
      }
      return Promise.reject(error);
    },
  );
};
