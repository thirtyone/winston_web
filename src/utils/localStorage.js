export const useLocalStorage = () => {
  const setLocalStorage = (key, value) => {
    localStorage.setItem(key, value);
  };
  const getLocalStorage = (key) => {
    const value = localStorage.getItem(key);
    return value;
  };

  const removeLocalStorage = (key) => {
    localStorage.removeItem(key);
  };

  const clearLocalStorage = () => {
    localStorage.clear();
  };
  return {
    setLocalStorage,
    getLocalStorage,
    removeLocalStorage,
    clearLocalStorage,
  };
};
